let gamingArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const textBoxEl = document.querySelector('#textBox');

const fieldOneEl = document.querySelector('#fieldOne');
fieldOneEl.addEventListener('click', () => {
    if (gamingArray[0] !== true && gamingArray[0] !== false) {
        gamingArray[0] = true;
        computerMove();
        renderGameWindow();
        isWinner()
    }
});
const fieldTwoEl = document.querySelector('#fieldTwo');
fieldTwoEl.addEventListener('click', () => {
    if (gamingArray[1] !== true && gamingArray[1] !== false) {
        gamingArray[1] = true;
        computerMove();
        renderGameWindow();
        isWinner()
    }
});
const fieldThreeEl = document.querySelector('#fieldThree');
fieldThreeEl.addEventListener('click', () => {
    if (gamingArray[2] !== true && gamingArray[2] !== false) {
        gamingArray[2] = true;
        computerMove();
        renderGameWindow();
        isWinner()
    }
});
const fieldFourEl = document.querySelector('#fieldFour');
fieldFourEl.addEventListener('click', () => {
    if (gamingArray[3] !== true && gamingArray[3] !== false) {
        gamingArray[3] = true;
        computerMove();
        renderGameWindow();
        isWinner()
    }
});
const fieldFiveEl = document.querySelector('#fieldFive');
fieldFiveEl.addEventListener('click', () => {
    if (gamingArray[4] !== true && gamingArray[4] !== false) {
        gamingArray[4] = true;
        computerMove();
        renderGameWindow();
        isWinner()
    }
});
const fieldSixEl = document.querySelector('#fieldSix');
fieldSixEl.addEventListener('click', () => {
    if (gamingArray[5] !== true && gamingArray[5] !== false) {
        gamingArray[5] = true;
        computerMove();
        renderGameWindow();
        isWinner()
    }
});
const fieldSevenEl = document.querySelector('#fieldSeven');
fieldSevenEl.addEventListener('click', () => {
    if (gamingArray[6] !== true && gamingArray[6] !== false) {
        gamingArray[6] = true;
        computerMove();
        renderGameWindow();
        isWinner()
    }
});
const fieldEightEl = document.querySelector('#fieldEight');
fieldEightEl.addEventListener('click', () => {
    if (gamingArray[7] !== true && gamingArray[7] !== false) {
        gamingArray[7] = true;
        computerMove();
        renderGameWindow();
        isWinner()
    }
});
const fieldNineEl = document.querySelector('#fieldNine');
fieldNineEl.addEventListener('click', () => {
    if (gamingArray[8] !== true && gamingArray[8] !== false) {
        gamingArray[8] = true;
        computerMove();
        renderGameWindow();
        isWinner()
    }
});

let winnerFlag = false;

function isWinner() {

    if (gamingArray[0] === true && gamingArray[1] === true && gamingArray[2] === true) {
        textBoxEl.innerHTML = `
        <span class="badge badge-success"><h4>Вы выиграли!</h4></span>
`;
        winnerFlag = true;

    }
    if (gamingArray[0] === false && gamingArray[1] === false && gamingArray [2] === false) {
        textBoxEl.innerHTML = `
        <span class="badge badge-danger"><h4>Вы проиграли</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[3] === true && gamingArray[4] === true && gamingArray[5] === true) {
        textBoxEl.innerHTML = `
        <span class="badge badge-success"><h4>Вы выиграли!</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[3] === false && gamingArray[4] === false && gamingArray [5] === false) {
        textBoxEl.innerHTML = `
        <span class="badge badge-danger"><h4>Вы проиграли</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[6] === true && gamingArray[7] === true && gamingArray[8] === true) {
        textBoxEl.innerHTML = `
        <span class="badge badge-success"><h4>Вы выиграли!</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[6] === false && gamingArray[7] === false && gamingArray [8] === false) {
        textBoxEl.innerHTML = `
        <span class="badge badge-danger"><h4>Вы проиграли</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[0] === true && gamingArray[3] === true && gamingArray[6] === true) {
        textBoxEl.innerHTML = `
        <span class="badge badge-success"><h4>Вы выиграли!</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[0] === false && gamingArray[3] === false && gamingArray [6] === false) {
        textBoxEl.innerHTML = `
        <span class="badge badge-danger"><h4>Вы проиграли</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[1] === true && gamingArray[4] === true && gamingArray[7] === true) {
        textBoxEl.innerHTML = `
        <span class="badge badge-success"><h4>Вы выиграли!</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[1] === false && gamingArray[4] === false && gamingArray [7] === false) {
        textBoxEl.innerHTML = `
        <span class="badge badge-danger"><h4>Вы проиграли</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[2] === true && gamingArray[5] === true && gamingArray[8] === true) {
        textBoxEl.innerHTML = `
        <span class="badge badge-success"><h4>Вы выиграли!</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[2] === false && gamingArray[5] === false && gamingArray [8] === false) {
        textBoxEl.innerHTML = `
        <span class="badge badge-danger"><h4>Вы проиграли</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[0] === true && gamingArray[4] === true && gamingArray[8] === true) {
        textBoxEl.innerHTML = `
        <span class="badge badge-success"><h4>Вы выиграли!</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[0] === false && gamingArray[4] === false && gamingArray [8] === false) {
        textBoxEl.innerHTML = `
        <span class="badge badge-danger"><h4>Вы проиграли</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[2] === true && gamingArray[4] === true && gamingArray[6] === true) {
        textBoxEl.innerHTML = `
        <span class="badge badge-success"><h4>Вы выиграли!</h4></span>
`;
        winnerFlag = true;
    }
    if (gamingArray[2] === false && gamingArray[4] === false && gamingArray [6] === false) {
        textBoxEl.innerHTML = `
        <span class="badge badge-danger"><h4>Вы проиграли</h4></span>
`;
        winnerFlag = true;
    }

}

function computerMove() {
    let availableArray = [];
    for (const number of gamingArray) {
        if (number !== true && number !== false) {
            availableArray.push(number)
        }
    }
    let randomNumber;
    randomNumber = Math.floor(Math.random() * availableArray.length);
    gamingArray[availableArray[randomNumber] - 1] = false;
}

function renderGameWindow() {
    if (gamingArray[0] === false) {
        fieldOneEl.textContent = 0;
    }
    if (gamingArray[0] === true) {
        fieldOneEl.textContent = '✕';
    }

    if (gamingArray[1] === false) {
        fieldTwoEl.textContent = 0;
    }
    if (gamingArray[1] === true) {
        fieldTwoEl.textContent = '✕';
    }
    if (gamingArray[2] === false) {
        fieldThreeEl.textContent = 0;
    }
    if (gamingArray[2] === true) {
        fieldThreeEl.textContent = '✕';
    }
    if (gamingArray[3] === false) {
        fieldFourEl.textContent = 0;
    }
    if (gamingArray[3] === true) {
        fieldFourEl.textContent = '✕';
    }
    if (gamingArray[4] === false) {
        fieldFiveEl.textContent = 0;
    }
    if (gamingArray[4] === true) {
        fieldFiveEl.textContent = '✕';
    }
    if (gamingArray[5] === false) {
        fieldSixEl.textContent = 0;
    }
    if (gamingArray[5] === true) {
        fieldSixEl.textContent = '✕';
    }
    if (gamingArray[6] === false) {
        fieldSevenEl.textContent = 0;
    }
    if (gamingArray[6] === true) {
        fieldSevenEl.textContent = '✕';
    }
    if (gamingArray[7] === false) {
        fieldEightEl.textContent = 0;
    }
    if (gamingArray[7] === true) {
        fieldEightEl.textContent = '✕';
    }
    if (gamingArray[8] === false) {
        fieldNineEl.textContent = 0;
    }
    if (gamingArray[8] === true) {
        fieldNineEl.textContent = '✕';
    }

    if (gamingArray[0] !== false && gamingArray[0] !== true) {
        fieldOneEl.textContent = null;
    }
    if (gamingArray[1] !== false && gamingArray[1] !== true) {
        fieldTwoEl.textContent = null;
    }

    if (gamingArray[2] !== false && gamingArray[2] !== true) {
        fieldThreeEl.textContent = null;
    }
    if (gamingArray[3] !== false && gamingArray[3] !== true) {
        fieldFourEl.textContent = null;
    }
    if (gamingArray[4] !== false && gamingArray[4] !== true) {
        fieldFiveEl.textContent = null;
    }
    if (gamingArray[5] !== false && gamingArray[5] !== true) {
        fieldSixEl.textContent = null;
    }
    if (gamingArray[6] !== false && gamingArray[6] !== true) {
        fieldSevenEl.textContent = null;
    }
    if (gamingArray[7] !== false && gamingArray[7] !== true) {
        fieldEightEl.textContent = null;
    }
    if (gamingArray[8] !== false && gamingArray[8] !== true) {
        fieldNineEl.textContent = null;
    }
}

const playAgainButtonEl = document.querySelector('#playAgainButton');
playAgainButtonEl.addEventListener('click', () => {
    gamingArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    renderGameWindow();
    textBoxEl.innerHTML = '';
});
